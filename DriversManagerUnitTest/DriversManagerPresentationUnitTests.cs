﻿using System;
using System.Collections.Generic;
using DriversManager;
using DriversManager.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace DriversManagerUnitTest
{
    [TestClass]
    public class DriversManagerPresentationUnitTests
    {
        private static Mock<SystemDriversModel> _systemDriversModelMock;

        private static readonly List<Dictionary<string, string>> testDriversList = new List<Dictionary<string, string>>
        {
            new Dictionary<string, string>
            {
                {"SERVICE_NAME", "srv1"},
                {"DISPLAY_NAME", "service1"},
                {"TYPE", "1  KERNEL_DRIVER"},
                {"STATE", "1 STOPPED"},
                {"STATES", "STOPPABLE, NOT_PAUSABLE, ACCEPTS_PRESHUTDOWN"}
            },
            new Dictionary<string, string>
            {
                {"SERVICE_NAME", "srv2"},
                {"DISPLAY_NAME", "service2"},
                {"TYPE", "some_noise_ddfqwr"},
                {"STATE", "some_noise_12edw"},
                {"STATES", "NOT_STOPPABLE, NOT_PAUSABLE, ACCEPTS_PRESHUTDOWN"}
            }
        };

        private static readonly List<Dictionary<string, string>> updatedDriversList = new List<Dictionary<string, string>>
        {
            new Dictionary<string, string>
            {
                {"SERVICE_NAME", "srv3"},
                {"DISPLAY_NAME", "service3"},
                {"TYPE", "2 SYSTEML_DRIVER"},
                {"STATE", "4 RUNNING"},
                {"STATES", "STOPPABLE, NOT_PAUSABLE, ACCEPTS_PRESHUTDOWN"}
            }
        };

        [ClassInitialize]
        public static void ClassInit(TestContext context)
        {
            _systemDriversModelMock = new Mock<SystemDriversModel>(MockBehavior.Strict);
            _systemDriversModelMock.Setup(m => m.GetDrivers()).Returns(testDriversList);
        }


        [TestMethod]
        public void ShouldConvertDriversModelsToPresentationItems()
        {
            //act
            var testerDriversPresentation = new DriversPresentation(_systemDriversModelMock.Object);

            //assert
            _systemDriversModelMock.Verify(m => m.GetDrivers());
            Assert.AreEqual(2, testerDriversPresentation.Drivers.Count);
            Assert.AreEqual("srv1", testerDriversPresentation.Drivers[0].ServiceVame);
            Assert.AreEqual("service1", testerDriversPresentation.Drivers[0].DisplayVame);
            Assert.AreEqual(DriverType.KernelDriver, testerDriversPresentation.Drivers[0].DriverType);
            Assert.AreEqual(DriverState.Stopped, testerDriversPresentation.Drivers[0].State);
            Assert.AreEqual("STOPPABLE, NOT_PAUSABLE, ACCEPTS_PRESHUTDOWN", testerDriversPresentation.Drivers[0].States);
            Assert.AreEqual("srv2", testerDriversPresentation.Drivers[1].ServiceVame);
            Assert.AreEqual("service2", testerDriversPresentation.Drivers[1].DisplayVame);
            Assert.AreEqual(DriverType.Unknown, testerDriversPresentation.Drivers[1].DriverType);
            Assert.AreEqual(DriverState.Unknown, testerDriversPresentation.Drivers[1].State);
            Assert.AreEqual("NOT_STOPPABLE, NOT_PAUSABLE, ACCEPTS_PRESHUTDOWN", testerDriversPresentation.Drivers[1].States);
        }

        [TestMethod]
        public void ShouldScheduleDriverStop()
        {
            //arrange
            _systemDriversModelMock.Setup(m => m.ScheduleDriverToStop("driverToStop", new DateTime(2500, 1, 1)))
                .Verifiable();
            var testerDriversPresentation = new DriversPresentation(_systemDriversModelMock.Object)
            {
                DriverToStop = "driverToStop",
                ScheduledDriverStopTime = new DateTime(2500, 1, 1)
            };
            testerDriversPresentation.ScheduledDriverStopTime = new DateTime(12345);

            //act
            testerDriversPresentation.Execute("ScheduleClicked");

            //assert
            _systemDriversModelMock.Verify(m => m.ScheduleDriverToStop("driverToStop", new DateTime(2500, 1, 1)));
        }

        [TestMethod]
        public void ShouldUpdateDriversOnUpdateDriversEvent()
        {
            //arrange
            var testerDriversPresentation = new DriversPresentation(_systemDriversModelMock.Object);
            var driversUpdatedCount = 0;
            var driversNamesUpdatedCount = 0;
            testerDriversPresentation.PropertyChanged += (sender, args) =>
            {
                if (args.PropertyName.Equals("Drivers")) driversUpdatedCount++;
                if (args.PropertyName.Equals("DriversNames")) driversNamesUpdatedCount++;
            };
            //act
            _systemDriversModelMock.Setup(m => m.GetDrivers()).Returns(updatedDriversList);
            _systemDriversModelMock.Raise(mock => mock.DriversUpdated += null);

            //assert
            Assert.AreEqual(1, driversUpdatedCount);
            Assert.AreEqual(1, driversNamesUpdatedCount);
            Assert.AreEqual(1, testerDriversPresentation.Drivers.Count);
            Assert.AreEqual("srv3", testerDriversPresentation.Drivers[0].ServiceVame);
            Assert.AreEqual("service3", testerDriversPresentation.Drivers[0].DisplayVame);
            Assert.AreEqual(DriverType.SystemDriver, testerDriversPresentation.Drivers[0].DriverType);
            Assert.AreEqual(DriverState.Running, testerDriversPresentation.Drivers[0].State);
            Assert.AreEqual("STOPPABLE, NOT_PAUSABLE, ACCEPTS_PRESHUTDOWN", testerDriversPresentation.Drivers[0].States);
        }

        [TestMethod]
        public void ShouldUpdateUpdateSecondsTillStopOnDriverStopCountdownTick()
        {
            //arrange
            var testerDriversPresentation = new DriversPresentation(_systemDriversModelMock.Object);
            var scheduleButtonTextUpdatedCount = 0;
            testerDriversPresentation.PropertyChanged += (sender, args) =>
            {
                if (args.PropertyName.Equals("ScheduleButtonText")) scheduleButtonTextUpdatedCount++;
            };

            //act
            _systemDriversModelMock.Raise(mock => mock.DriverStopCountdownTick += null, new DriversModel.CountdownTickEventArgs {SecondsTillStop = "12"});

            //assert
            Assert.AreEqual(1, scheduleButtonTextUpdatedCount);
            Assert.AreEqual("12", testerDriversPresentation.ScheduleButtonText);
        }

        [TestMethod]
        public void ShouldProcessDriverStopExecutedCorrectly()
        {
            //arrange
            var testerDriversPresentation = new DriversPresentation(_systemDriversModelMock.Object)
            {
                ScheduleEnabled = false,
                ScheduleButtonText = "",
                DriverToStop = ""
            };
            var scheduleButtonTextUpdatedCount = 0;
            var scheduleEnabledUpdatedCount = 0;
            var driverToStopUpdatedCount = 0;
            var driversUpdatedCount = 0;
            var driversNamesUpdatedCount = 0;
            testerDriversPresentation.PropertyChanged += (sender, args) =>
            {
                if (args.PropertyName.Equals("ScheduleButtonText")) scheduleButtonTextUpdatedCount++;
                if (args.PropertyName.Equals("ScheduleEnabled")) scheduleEnabledUpdatedCount++;
                if (args.PropertyName.Equals("DriverToStop")) driverToStopUpdatedCount++;
                if (args.PropertyName.Equals("Drivers")) driversUpdatedCount++;
                if (args.PropertyName.Equals("DriversNames")) driversNamesUpdatedCount++;
            };

            //act
            _systemDriversModelMock.Raise(mock => mock.DriverStopExecuted += null);

            //assert
            Assert.AreEqual(1, driversUpdatedCount);
            Assert.AreEqual(1, driversNamesUpdatedCount);
            Assert.AreEqual(1, scheduleButtonTextUpdatedCount);
            Assert.AreEqual(1, scheduleEnabledUpdatedCount);
            Assert.AreEqual(1, driverToStopUpdatedCount);
            Assert.AreEqual("Schedule Driver Stop", testerDriversPresentation.ScheduleButtonText);
            Assert.AreEqual(true, testerDriversPresentation.ScheduleEnabled);
            Assert.AreEqual(testerDriversPresentation.DriversNames[0], testerDriversPresentation.DriverToStop);
        }
    }
}