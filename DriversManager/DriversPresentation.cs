﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows.Input;
using DriversManager.Annotations;
using DriversManager.Model;
using DriversManager.Properties;

namespace DriversManager
{
    public class DriversPresentation : INotifyPropertyChanged, ICommand
    {
        private const string ScheduleStop = "Schedule Driver Stop";
        private const string StopScheduled = "Driver Stop Scheduled";

        private readonly DriversModel _driversModel;

        private ObservableCollection<DriverPresentation> _drivers;
        private string _driverToStop;
        private string _scheduleButtonText = ScheduleStop;

        private DateTime _scheduledDriverStopTime = DateTime.Now;
        private bool _scheduleEnabled = true;


        public DriversPresentation(DriversModel driversModel)
        {
            _driversModel = driversModel;
            Drivers = new ObservableCollection<DriverPresentation>();
            ScanDrivers();


            driversModel.DriversUpdated += OnDriversUpdated;
            driversModel.DriverStopCountdownTick += OnStopCountdownTick;
            driversModel.DriverStopExecuted += OnDriverStopExecuted;

            DriverToStop = DriversNames[0];

            if (Settings.Default.StopDriverName != "" && Settings.Default.TicksToStop != 0)
            {
                if (Drivers.Single(s => s.ServiceVame == Settings.Default.StopDriverName).States.StartsWith("STOPPABLE"))
                {
                    DriverToStop = Settings.Default.StopDriverName;
                    ScheduledDriverStopTime = new DateTime(Settings.Default.TicksToStop);
                    Execute("ScheduleClicked");
                }
                else
                {
                    DriverToStop = DriversNames[0];
                }
            }
        }

        public ObservableCollection<DriverPresentation> Drivers
        {
            get => _drivers;
            set
            {
                _drivers = value;
                OnPropertyChanged(nameof(Drivers));
                OnPropertyChanged(nameof(DriversNames));
            }
        }

        public List<string> DriversNames => _drivers.Where(
            d => d.States.StartsWith("STOPPABLE") & d.State == DriverState.Running)
            .Select(d => d.ServiceVame).ToList();

        public DateTime ScheduledDriverStopTime
        {
            get => _scheduledDriverStopTime;
            set
            {
                if (DateTime.Compare(value, DateTime.Now) < 0) return;
                _scheduledDriverStopTime = value;
                OnPropertyChanged(nameof(ScheduledDriverStopTime));
            }
        }

        public string DriverToStop
        {
            get => _driverToStop;
            set
            {
                _driverToStop = value;
                OnPropertyChanged(nameof(DriverToStop));
            }
        }

        public bool ScheduleEnabled
        {
            get => _scheduleEnabled;
            set
            {
                _scheduleEnabled = value;
                OnPropertyChanged(nameof(ScheduleEnabled));
            }
        }

        public string ScheduleButtonText
        {
            get => _scheduleButtonText;
            set
            {
                _scheduleButtonText = value;
                OnPropertyChanged(nameof(ScheduleButtonText));
            }
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            if (!(parameter is string) || !parameter.Equals("ScheduleClicked") ||
                DateTime.Now > ScheduledDriverStopTime) return;
            ScheduleEnabled = false;
            ScheduleButtonText = StopScheduled;
            _driversModel.ScheduleDriverToStop(DriverToStop, ScheduledDriverStopTime);
        }

        public event EventHandler CanExecuteChanged;

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnDriverStopExecuted()
        {
            ScheduleEnabled = true;
            ScheduleButtonText = ScheduleStop;
            DriverToStop = DriversNames[0];
            ScanDrivers();
        }

        private void OnStopCountdownTick(object sender, DriversModel.CountdownTickEventArgs e)
        {
            ScheduleButtonText = e.SecondsTillStop;
        }

        private void OnDriversUpdated()
        {
            ScanDrivers();
        }

        private void ScanDrivers()
        {
            var scannedDrivers = new ObservableCollection<DriverPresentation>();

            foreach (var dM in _driversModel.GetDrivers())
            {
                var dp = new DriverPresentation
                {
                    ServiceVame = dM["SERVICE_NAME"],
                    DisplayVame = dM["DISPLAY_NAME"],
                    State = GetDriverState(dM["STATE"]),
                    DriverType = GetDriverType(dM["TYPE"]),
                    States = dM.ContainsKey("STATES") ? dM["STATES"] : ""
                };

                scannedDrivers.Add(dp);
            }

            Drivers = scannedDrivers;
        }

        private static DriverState GetDriverState(string s)
        {
            DriverState result;
            switch (s[0])
            {
                case '1':
                    result = DriverState.Stopped;
                    break;
                case '2':
                    result = DriverState.StartPending;
                    break;
                case '3':
                    result = DriverState.Stoping;
                    break;
                case '4':
                    result = DriverState.Running;
                    break;
                case '5':
                    result = DriverState.ContinuePending;
                    break;
                case '6':
                    result = DriverState.PausePending;
                    break;
                case '7':
                    result = DriverState.Paused;
                    break;

                default:
                    result = DriverState.Unknown;
                    break;
            }

            return result;
        }


        private static DriverType GetDriverType(string s)
        {
            DriverType result;
            switch (s[0])
            {
                case '1':
                    result = DriverType.KernelDriver;
                    break;
                case '2':
                    result = DriverType.SystemDriver;
                    break;
                case '8':
                    result = DriverType.RecognizedDriver;
                    break;
                default:
                    result = DriverType.Unknown;
                    break;
            }

            return result;
        }

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }

    public class DriverPresentation
    {
        public string ServiceVame { get; set; }
        public DriverState State { get; set; }
        public string DisplayVame { get; set; }
        public DriverType DriverType { get; set; }
        public string States { get; set; }
    }

    public enum DriverState
    {
        Stopped = 1,
        StartPending = 2,
        Stoping = 3,
        Running = 4,
        ContinuePending = 5,
        PausePending = 6,
        Paused = 7,
        Unknown = 255
    }

    public enum DriverType
    {
        KernelDriver = 1,
        SystemDriver = 2,
        RecognizedDriver = 8,
        Unknown = 255
    }
}