﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Windows.Threading;
using DriversManager.Properties;

namespace DriversManager.Model
{
    public class SystemDriversModel : DriversModel
    {
        private static List<Dictionary<string, string>> _drivers;
        private static bool _newDriverInfoFound;
        private static Dictionary<string, string> _driverInfo;

        private readonly DispatcherTimer _driverStopScheduleTimer;
        private readonly TimeSpan _driversUpdateTimeout = new TimeSpan(0, 0, 0, 10);

        private string _driverToStop;
        private DateTime _scheduledDriverStopTime;

        public SystemDriversModel()
        {
            _drivers = new List<Dictionary<string, string>>();
            GetSystemDrivers();

            var driversRefreshTimer = new DispatcherTimer();
            driversRefreshTimer.Tick += DriversRefreshTimerTick;
            driversRefreshTimer.Interval = _driversUpdateTimeout;
            driversRefreshTimer.Start();

            _driverStopScheduleTimer = new DispatcherTimer();
            _driverStopScheduleTimer.Tick += DriverStopScheduleTimerElapsed;
            _driverStopScheduleTimer.Interval = _driversUpdateTimeout;
        }

        public override List<Dictionary<string, string>> GetDrivers()
        {
            return _drivers;
        }

        private void DriverStopScheduleTimerElapsed(object sender, EventArgs e)
        {
            if (DateTime.Now < _scheduledDriverStopTime)
            {
                InvokeDriverStopCountdownTick(new CountdownTickEventArgs {SecondsTillStop = Math.Truncate((DateTime.Now - _scheduledDriverStopTime).TotalSeconds).ToString(CultureInfo.InvariantCulture)});
            }
            else
            {
                StopDriver(_driverToStop);
                InvokeDriverStopExecuted();
                _driverStopScheduleTimer.Stop();
            }
        }

        private void DriversRefreshTimerTick(object sender, EventArgs e)
        {
            _drivers.Clear();
            GetSystemDrivers();
        }

        private void GetSystemDrivers()
        {
            RunCmdAndGetOutput("sc query type= driver state= all", DriverQuery_DataReceived, cmd_Error);
        }

        private static void StopDriver(string driverToStop)
        {
            RunCmdAndGetOutput("net stop " + driverToStop + " /yes", stopDriver_DataReceived, cmd_Error);
        }

        public override void ScheduleDriverToStop(string driverName, DateTime scheduledDriverStopTime)
        {
            _driverToStop = driverName;
            _driverStopScheduleTimer.Interval = new TimeSpan(0, 0, 0, 1);
            _driverStopScheduleTimer.Start();

            _scheduledDriverStopTime = scheduledDriverStopTime;
            Settings.Default.TicksToStop = scheduledDriverStopTime.Ticks;
            Settings.Default.StopDriverName = driverName;
            Settings.Default.Save();
        }

        private static void RunCmdAndGetOutput(string lineToRun, DataReceivedEventHandler dataReceived,
            DataReceivedEventHandler errorOccured)
        {
            var cmdStartInfo = new ProcessStartInfo
            {
                FileName = @"C:\Windows\System32\cmd.exe",
                RedirectStandardOutput = true,
                RedirectStandardError = true,
                RedirectStandardInput = true,
                UseShellExecute = false,
                CreateNoWindow = true
            };

            var cmdProcess = new Process
            {
                StartInfo = cmdStartInfo
            };

            cmdProcess.ErrorDataReceived += errorOccured;
            cmdProcess.OutputDataReceived += dataReceived;
            cmdProcess.EnableRaisingEvents = true;
            cmdProcess.Start();
            cmdProcess.BeginOutputReadLine();
            cmdProcess.BeginErrorReadLine();
            cmdProcess.StandardInput.WriteLine(lineToRun);
            cmdProcess.StandardInput.WriteLine("exit");
            cmdProcess.WaitForExit();
        }

        private void DriverQuery_DataReceived(object sender, DataReceivedEventArgs e)
        {
            if (!_newDriverInfoFound)
            {
                if (e.Data != null)
                {
                    if (e.Data.Split(' ')[0].Equals("SERVICE_NAME:"))
                    {
                        _newDriverInfoFound = true;
                        _driverInfo = new Dictionary<string, string>();
                        ScanDriverData(e.Data);
                    }
                }
                else
                {
                    InvokeDriversUpdated();
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(e.Data))
                {
                    ScanDriverData(e.Data);
                }
                else
                {
                    if (_driverInfo != null)
                    {
                        _drivers.Add(_driverInfo);
                        _newDriverInfoFound = false;
                    }
                }
            }
        }

        private static void stopDriver_DataReceived(object sender, DataReceivedEventArgs e)
        {
            Console.WriteLine(e.Data);
        }

        private static void cmd_Error(object sender, DataReceivedEventArgs e)
        {
            Console.WriteLine(@"Error from other process");
            Console.WriteLine(e.Data);
        }

        private static void ScanDriverData(string eData)
        {
            if (eData.Trim().StartsWith("("))
                _driverInfo.Add("STATES", eData.Trim().Substring(1, eData.Trim().Length - 2));
            else
                _driverInfo.Add(eData.Substring(0, eData.LastIndexOf(':')).Trim(),
                    eData.Substring(eData.LastIndexOf(':') + 1).Trim());
        }
    }
}