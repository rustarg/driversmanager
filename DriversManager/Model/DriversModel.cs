﻿using System;
using System.Collections.Generic;

namespace DriversManager.Model
{
    public abstract class DriversModel
    {
        public abstract List<Dictionary<string, string>> GetDrivers();

        public virtual event Action DriversUpdated;

        public virtual event Action DriverStopExecuted;

        public virtual event EventHandler<CountdownTickEventArgs> DriverStopCountdownTick;

        protected void InvokeDriversUpdated()
        {
            DriversUpdated?.Invoke();
        }

        protected void InvokeDriverStopExecuted()
        {
            DriverStopExecuted?.Invoke();
        }

        protected void InvokeDriverStopCountdownTick(CountdownTickEventArgs e)
        {
            DriverStopCountdownTick?.Invoke(this, e);
        }

        public abstract void ScheduleDriverToStop(string driverName, DateTime scheduledDriverStopTime);


        public class CountdownTickEventArgs : EventArgs
        {
            public string SecondsTillStop { get; set; }
        }
    }
}