# Drivers Manager Application

## Requirements (from Karin) 

Upload your code to a public git repo like Github or Bitbucket
Do not commit all your code in one go, especially if you use generated files, keep in mind we want to understand what you wrote
Please document your task


Windows drivers listing and scheduling

Write a C#.NET program that lists all the drivers in the system with their status (gets updated every 10 seconds).

Allow to set a schedule for stopping one of them (assuming the program is up at that time).

Make sure to save the schedule so that it persists between consecutive runs of the application.

## Solution


- Application logic should implement the wrapper on console commands
- Use WPF as presentation technology
- Business logic should be covered by tests 
- Code should be uploaded to Bitbucket repository

## Application user overview
- Start application `DriversManager.exe` with administrative privileges(!)
- ![Application UI](ui.png)

- There are Driver Stop Scheduler on top of application window and Drivers List below it.
- To shedule driver to stop, please 
  - select required driver in schedulers dropdown box
  - pick the date time when to stop the driver
  - Press the `Schedule Driver Stop` button
- then countdown will start, and time in seconds till driver stop will shown on disabled button as caption


## Application technical overview

### Model

- Used MVP approach. Main logic is under `DriversModel` `SystemDriversModel` classes.
- To get list of system drivers executed internal `cmd.exe` process with `sc query type= driver state= all` command.
- To stop a specific driver  executed internal `cmd.exe` process with `"net stop driverToStop  /yes"` command.
- The output is parsed and stored in  `List<Dictionary<string, string>>` as drivers model.  
- The `driversRefreshTimer` refreshes the drivers list every 10 sec (regarding requirements)
- `ScheduleDriverToStop` method schedules driver stopping by starting  the  `_driverStopScheduleTimer`

### View

Consists of `Scheduler` and `Drivers Grid`
- Scheduler
  - In Scheduler there is a Combo-Box to select driver to stop,
Date-picker tool to pick desired date time when try to stop the driver.
  - `ScheduleButton` to schedule driver stop event.

- Drivers Grid
  - Shows the all system drivers returned by `sc.exe` tool
  - Is read only
  - Is bound to `Presentation` with ItemsSource property 
  

### Presentation

- Drivers Grid Presentation
  - `ObservableCollection<DriverPresentation>() Drivers` holds representational information about shown drivers in the data grid

- Scheduler Presentation
  - `DriversNamesList` is  Combo-Box to select driver items source. It contains only drivers, that are currently running and have `STOPPABLE` state
  - In `Date-picker` is not possible to select date from the past.
  - `ScheduleButton` is disabled after scheduling and shows time in seconds till application will try to stop selected driver 
  - `DriverToStop` and `ScheduledDriverStopTime` variables are persisted in application settings, and restored during application consequent launch.

### UnitTests
- used MSVS TestPlatform and `moq` framework for mocking
- covered most of view and model interaction specific logic.


